/* ========== NAVBAR ============= */

// for small screen toggle
$(document).ready(function () {
    let mainNav = document.getElementById('main-nav');
    let navbarToggle = document.getElementById('navbar-toggle');

    navbarToggle.addEventListener('click', function () {

        if (this.classList.contains('active')) {
            mainNav.style.display = "none";
            this.classList.remove('active');
        } else {
            mainNav.style.display = "flex";
            this.classList.add('active');

        }
    });
});

// change color on scroll
$(document).ready(function () {
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        var win = $(window).width();
        if (scroll < 100) {
            $(".navbar").css("background", "transparent");
        } else {
            $(".navbar").css({
                'background': '#22292f',
                'transition': '0.5s'
            });
        }
    });
    $('#navbar-toggle').click(function() {
        var background = '';
        if($('#main-nav').is(':visible')) background = '#22292f';
        else background = 'transparent';
        $('.navbar').css({
            'background': background,
            'transition': '0.5s'
        });
    });
});
